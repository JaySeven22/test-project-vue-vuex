import { createApp, Vue } from 'vue'
import App from './App.vue'
import store from './store';


const app = createApp(App);
app.use(store);
app.mount('#app');

new Vue({
	el: '#app',

	store,

	beforeCreate() {
		this.$store.commit('initialiseStore');
	}
});