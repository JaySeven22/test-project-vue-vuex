import { createStore } from "vuex";
import axios from "axios";




const store = createStore({
    state() {
        return {
            products: [
                {
                    id: 1,
                    title: "G2H" ,
                    img: require('@/assets/Rectangle72.jpg'),
                    description: '12-72/168 м3/ч / гидрорегулируемый расход / от датчика присутствия' ,
                    cost: 12644 ,
                    articul: 'Артикул: G2H1065 ',
                    counter: 1
                },
                {
                    id: 2,
                    title: 'BXC',
                    img: require('@/assets/Rectangle72(1).jpg'),
                    description: '12-72/168 м3/ч / гидрорегулируемый расход / от датчика присутствия' ,
                    cost: 12644 ,
                    articul: 'Артикул: G2H1065' ,
                    counter: 1
                },
                {
                    id: 3,
                    title: 'GHN' ,
                    img:  require('@/assets/Rectangle72(2).jpg'),
                    description: '12-72/168 м3/ч / гидрорегулируемый расход / от датчика присутствия' ,
                    cost:  12644 ,
                    articul: 'Артикул: G2H1065' ,
                    counter: 2
                }
            ],
            install: false,
            viewedProducts: [
                {   
                    id: 2,
                    title: "BXC" ,
                    img: require('@/assets/BXC-pre-mur-1-modif-BD-768x768 1.png'),
                    description: 'Вытяжное устройство для механической системы вентиляции' ,
                    priceRange: '6 848 ₽ – 56 584 ₽ '  ,
                    costEuro: '77.60 € – 643.86 €',
                    articul: 'Артикул: G2H1065 ',
                    cost:  12644 ,
                    counter: 1
                },
                {   
                    id: 1,
                    title: "G2H" ,
                    img: require('@/assets/BXC-pre-mur-1-modif-BD-768x768 2.png'),
                    description: 'Многофункциональное вытяжное устройство для естественной и гибридной вентиляции' ,
                    priceRange: '6 848 ₽ – 56 584 ₽ '  ,
                    costEuro: '77.60 € – 643.86 €',
                    articul: 'Артикул: G2H1065 ',
                    cost:  12644 ,
                    counter: 1
                },
                {   
                    id: 3,
                    title: "GHN" ,
                    img:  require('@/assets/BXC-pre-mur-1-modif-BD-768x768 3.png'),
                    description: 'Вытяжное устройство с датчиком присутствия' ,
                    priceRange: '6 848 ₽ – 56 584 ₽ '  ,
                    costEuro: '77.60 € – 643.86 €',
                    articul: 'Артикул: G2H1065 ',
                    cost:  12644 ,
                    counter: 1
                },
                {   
                    id: 4,
                    title: "TDA" ,
                    img: require('@/assets/BXC-pre-mur-1-modif-BD-768x768 4.png'),
                    description: 'Вытяжное устройство с датчиком присутствия' ,
                    priceRange: '6 848 ₽ – 56 584 ₽ '  ,
                    costEuro: '77.60 € – 643.86 €',
                    articul: 'Артикул: G2H1065 ',
                    cost:  12644 ,
                    counter: 1
                }
            ],
            modalItem: null,
            booleanModal: false
        }
    },
    mutations: {
            initialiseStore(state) {
                // Check if the ID exists
                if(localStorage.getItem('store')) {
                    // Replace the state object with the stored item
                    this.replaceState(
                        Object.assign(state, JSON.parse(localStorage.getItem('store')))
                    );
                }
            },
            clearBasket(state) {
                return state.products.splice(0, state.products.length)
            },
            checkboxWatch(state) {
                return state.install = !state.install
            },
            addProducts(state) {
                return state.products.push(state.modalItem)
            },
            increment(state, item) {
                return state.products.find(product => product.title == item.title).counter++
            },
            decrement(state, item) {
                return state.products.find(product => product.title == item.title).counter--
            },
            deleteProduct(state, item) {
                return state.products = state.products.filter(product => product.title !== item.title)
            }
        },
    getters: {
            getAllProducts(state) {
                return state.products        
            },
            getAllViewedProducts(state) {
                return state.viewedProducts
            }

        },
        actions: {
            loadingInServer(state) {
                axios.post('url', {
                    readyOrder: state.products,
                    install: state.install
                })
                state.products = null
            }
        }      
    }
    
)


store.subscribe((mutation, state) => {
	// Store the state object as a JSON string
	localStorage.setItem('store', JSON.stringify(state));
});



export default store;
